import os
import random

import cv2
import numpy as np
import pandas as pd
import torch
from PIL import Image
from torch.utils.data import Dataset
from trdg.generators import (GeneratorFromDict, GeneratorFromRandom,
                             GeneratorFromStrings, GeneratorFromWikipedia)


class ocrDataset(Dataset):
    def __init__(self, root_dir, file_name, in_channel = 3, transform=None):
        self.root_dir = root_dir
        (self.paths, self.labels) = self.read_data(root_dir, file_name)
        self.in_channel = in_channel
        self.transform = transform

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, index):
        try:
            file_name = os.path.join(self.root_dir, self.paths[index])
            img = Image.open(file_name)
        except IOError:
            print('Corrupted image for %d' % index)
            return self[index + 1]
        if self.in_channel == 1:
            img = img.convert('L')
        if self.transform is not None:
            img = self.transform(img)
        label = self.labels[index].encode()
        return (img, label)

    @staticmethod
    def read_data(root_dir, file_name):
        df = pd.read_csv(os.path.join(root_dir, file_name))
        paths = list()
        labels = list()
        for i in range(len(df)):
            paths.append(df.loc[i, 'image'])
            labels.append(df.loc[i, 'text'])
        return paths, labels


words = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
rand = lambda x: np.random.randint(len(x))
class generateDataset(Dataset):
    def __init__(self, in_channel=3, transform=None, albu_transform = None):
        rand_str = []
        for i in range(10000):
            rand_str.append(self.random_string())
        self.generator = GeneratorFromStrings(
            strings=rand_str,
            size=64,
            blur=2,
            random_blur=True,
            background_type=np.random.randint(6),
            # text_color="#CE1912"
        )
        self.in_channel = in_channel
        self.transform = transform
        self.albu_transform = albu_transform

    def __len__(self):
        return 10000
    def __getitem__(self, index):
        try:
            img, label = self.generator.next()
        except IOError:
            print('Corrupted image for %d' % index)
            return self[index + 1]
        
        if self.in_channel == 1:
            img = img.convert('L')
        if self.transform is not None:
            img = self.transform(img)
        
        if self.albu_transform is not None:
            augment = self.albu_transform(image=np.array(img))
            img = augment['image']
        
        label = label.encode()
        return (img, label)

    @staticmethod
    def random_string():
        return numbers[rand(numbers)]+numbers[rand(numbers)]+'-'+numbers[rand(numbers)]+numbers[rand(numbers)]+'-'+numbers[rand(numbers)]+numbers[rand(numbers)]+numbers[rand(numbers)]+numbers[rand(numbers)]+' '+words[rand(words)] + ' ' + numbers[rand(numbers)]+numbers[rand(numbers)]+':'+numbers[rand(numbers)]+numbers[rand(numbers)]+':'+numbers[rand(numbers)]+numbers[rand(numbers)]
