import torch
import random
import albumentations as albu 
from albumentations.pytorch.transforms import ToTensor
from albumentations.core.transforms_interface import ImageOnlyTransform
from albumentations.augmentations import functional as F  


class Resize_Pytoan(ImageOnlyTransform):
    def __init__(self, height, always_apply=False, p=0.5):
        super(Resize_Pytoan, self).__init__(always_apply, p)
        self.height = height 
    def apply(self, image, **params):
        h, w = image.shape[:2]
        w = w*self.height*1.0/h
        img = F.resize(image, width=int(w), height=int(self.height))
        return img 

def get_augumentation(phase, height=64):
    list_transforms = []
    list_transforms.extend([
            Resize_Pytoan(height=height, always_apply=True)
    ])
    if phase == 'train':
        list_transforms.extend([
            albu.augmentations.transforms.Rotate(limit=(-1, 1), p=0.4),
            albu.OneOf([
                albu.IAAAdditiveGaussianNoise(),
                albu.augmentations.transforms.GaussNoise(),
            ], p=0.2),
            albu.OneOf([
                albu.augmentations.transforms.MotionBlur(p=.2),
                albu.augmentations.transforms.MedianBlur(blur_limit=3, p=0.1),
                albu.augmentations.transforms.Blur(blur_limit=3, p=0.1),
            ], p=0.2),
            albu.OneOf([
                albu.augmentations.transforms.OpticalDistortion(p=0.3),
                albu.augmentations.transforms.GridDistortion(p=.1),
                albu.IAAPiecewiseAffine(p=0.3),
            ], p=0.2),
            albu.OneOf([
                albu.augmentations.transforms.CLAHE(clip_limit=2),
                albu.IAASharpen(),
                albu.IAAEmboss(),
                albu.augmentations.transforms.RandomBrightnessContrast(),            
            ], p=0.3),
            albu.augmentations.transforms.HueSaturationValue(p=0.3)
        ])
    
    list_transforms.extend([
        albu.Normalize(mean=(0.485, 0.456, 0.406),
                       std=(0.229, 0.224, 0.225), p=1),
        ToTensor()
    ])
    return albu.Compose(list_transforms)


class alignCollate(object):
    def __init__(self):
        pass
    def __call__(self, batch):
        (images, labels) = zip(*batch)
        c = images[0].size(0)
        h = max([p.size(1) for p in images])
        w = max([p.size(2) for p in images])
        batch_images = torch.zeros(len(images), c, h, w).fill_(1)
        for i, image in enumerate(images):
            started_h = max(0, random.randint(0, h-image.size(1)))
            started_w = max(0, random.randint(0, w-image.size(2)))
            batch_images[i,:, started_h:started_h+image.size(1), started_w:started_w+image.size(2)] = image
        return batch_images, labels
