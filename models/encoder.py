import torch
import torch.nn.functional as F
import torch.nn as nn
from torch.autograd import Variable
from modules.densenet import Densenet
from modules.efficientnet import EfficientNet

class Encoder(nn.Module):
    def __init__(self, in_channel, dropout_rate = 0.2, network='densenet'):
        super(Encoder, self).__init__()
        if network == 'densenet':
            self.model = Densenet(in_channel = in_channel, dropout_rate=dropout_rate)
        elif 'efficientnet' in network:
            self.model = EfficientNet.from_pretrained(network)
            self.model._conv_stem.in_channels=1 
            self.model._conv_stem.weight = torch.nn.Parameter(self.model._conv_stem.weight[:, :1, :, :])

    def forward(self, input):
        out = self.model(input)
#         print('out: ', out.size())
        return out
