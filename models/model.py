import torch 
import torch.nn as nn
import torch.nn.functional as F
from models.encoder import Encoder
from models.decoder import Decoder


class ocrModel(nn.Module):
    def __init__(self, num_classes, in_channel, fixed_height = 64, network='efficientnet'):
        super(ocrModel, self).__init__()
        self.encoder = Encoder(in_channel = in_channel, network = network)
        self.input_dim = -1
        if 'densenet' in network:
            self.input_dim = int(fixed_height * 288 / 8)
        elif 'efficientnet'  in network:
            self.input_dim = int(fixed_height*320/8)
        else:
            print('ERROR: Please choose architecture') 
        self.decoder = Decoder(input_dim=self.input_dim, num_class=num_classes)
        self.crnn = nn.Sequential(
            self.encoder,
            self.decoder
        )
        self.log_softmax = nn.LogSoftmax(dim=2)

    def forward(self, input):
        output = self.crnn(input)
        output = output.permute(1, 0, 2)
        output = self.log_softmax(output)
        return output


