import torch
import torch.nn as nn
from torch.nn import functional as F
from modules.bilstm import BidirectionalLSTM

class Decoder(nn.Module):
    def __init__(self, input_dim, hidden_dim = 512, num_class = 200):
        super(Decoder, self).__init__()
        self.rnn1 = BidirectionalLSTM(input_dim, hidden_dim, hidden_dim)
        self.rnn2 = BidirectionalLSTM(hidden_dim, hidden_dim, num_class)


    def forward(self, X):
        X = X.view(X.size(0), X.size(1)*X.size(2), X.size(3))
        X = X.permute(0, 2, 1)
        X = self.rnn1(X)
        output = self.rnn2(X)
        return output