import pandas as pd 
import os 
import subprocess
import copy
import datetime
import torch
def get_vocab(root_dir, file_name):
    df = pd.read_csv(os.path.join(root_dir, file_name))
    labels = df['label'].tolist()
    alphabets = ''.join(sorted(set(''.join(labels))))
    return alphabets

def get_gpu():                                                                                          
    df = pd.DataFrame()                                                                                 
    memory_used = subprocess.check_output([                                                             
            'nvidia-smi', '--query-gpu=memory.used',                                                    
            '--format=csv,nounits,noheader'                                                             
        ], encoding='utf-8')                                                                            
    memory_used = [int(x) for x in memory_used.strip().split('\n')]                                     
    df['used'] = memory_used                                                                            
    del memory_used                                                                                     
                                                                                                        
    memory_total = subprocess.check_output([                                                            
            'nvidia-smi', '--query-gpu=memory.total',                                                   
            '--format=csv,nounits,noheader'                                                             
        ], encoding='utf-8')                                                                            
    memory_total = [int(x) for x in memory_total.strip().split('\n')]                                   
    df['total'] = memory_total                                                                          
    del memory_total 

    memory_free = subprocess.check_output([                                                            
            'nvidia-smi', '--query-gpu=memory.free',                                                   
            '--format=csv,nounits,noheader'                                                             
        ], encoding='utf-8')                                                                            
    memory_free = [int(x) for x in memory_free.strip().split('\n')]  
    df['free'] = memory_free
    del memory_free
    df = df.reset_index()                                                                               
    df = df.sort_values(by=['used'])
    df = df.groupby(["used"]).apply(lambda x: x.sort_values(["free"], ascending = False)).reset_index(drop=True)
    result_id = 0                                                                                       
    if(df['used'][0]==0):                                                                               
        print('Yeah, no one used them')                                                                 
        result_id = df['index'][0]                                                                      
    else:                                                                                               
        result_id = df['index'][0]                                                                      
        print('Oh, has anyone used them')
    del df
    return int(result_id)

def get_state_dict(model):
    if type(model) == torch.nn.DataParallel:
        state_dict = model.module.state_dict()
    else:
        state_dict = model.state_dict()
    return state_dict
def save_checkpoint(args, epoch, model, save_dir, save_best, start_time):
    
    checkpoint_dir = os.path.join(save_dir, args.network, start_time)
    model = copy.deepcopy(model)
    state = {
        'parser': args,
        'epoch': epoch,
        'state_dict': get_state_dict(model),
    }
    filename = os.path.join(checkpoint_dir, 'checkpoint_epoch{}.pth'.format(epoch))
    torch.save(state, filename)
    print('Saving checkpoint: {} ...'.format(filename))
    if(save_best):
        best_path = os.path.join(checkpoint_dir, 'model_best.pth')
        torch.save(state, best_path)
        print('Saving current best: {} ...'.format('model_best.pth'))

def loadData(v, data):
    with torch.no_grad():
        v.resize_(data.size()).copy_(data)
