from datasets import ocrDataset, train_transforms, generateDataset
import cv2 
import os
import numpy as np
import torch
import matplotlib.pyplot as plt 
dataset = generateDataset(transform=train_transforms(height=64))

for i in range(20):
    img, label = dataset[i]
    print(img)
    print("label at {}: {}".format(i, label))
    # cv2.imwrite('./saved/{}.png'.format(label), img)
    plt.imshow(img)
    plt.show()