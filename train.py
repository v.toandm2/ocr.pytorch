import subprocess
import os
import argparse
import random
import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torch.optim as optim
from torch.autograd import Variable
from torch.nn import CTCLoss
import time
import datetime
import torch.nn as nn
from datasets import ocrDataset, generateDataset, train_transforms, test_transforms, alignCollate, get_augumentation

from models import ocrModel, denModel, CRNN
from utils import strLabelConverter, get_gpu, get_vocab, by_char, by_field, loadData, save_checkpoint

parser = argparse.ArgumentParser()

parser.add_argument('--dataset', default='generate', help='Choose dataset')
parser.add_argument('--root_dir', default='/home/aiuser/data/train', help='path to dataset')
parser.add_argument('--num_worker', type=int,
                    help='number of data loading workers', default=8)
parser.add_argument('--batch_size', type=int,
                    default=32, help='input batch size')
parser.add_argument('--hidden_size', type=int,
                    default=256, help='input hidden size')
parser.add_argument('--height', type=int, default=64,
                    help='the height of the input image to network')
parser.add_argument('--alphabet', type=str,
                    default=' 0123456789:-FMSTWadehinorut')
parser.add_argument('--num_class', type=int, default=64,
                    help='the number class of the input image to network')
parser.add_argument('--start_epoch', type=int, default=1,
                    help='start epoch')
parser.add_argument('--in_channel', type=int, default=3,
                    help='the number channel of the input image to network')
parser.add_argument('--num_epoch', type=int, default=500,
                    help='number of epochs to train for')
parser.add_argument('--lr', type=float,
                    default=0.0001, help='learning rate for neural network')
parser.add_argument('--resume', default=None,
                    help="path to pretrained model (to continue training)")
parser.add_argument('--save_dir', default='./saved',
                    help='Where to store samples and models')
parser.add_argument('--manual_seed', type=int,
                    default=1234, help='reproduce experiemnt')
parser.add_argument('--network', default='densenet',
              help='Choose model in training')

args = parser.parse_args()

start_time = datetime.datetime.now().strftime('%m-%d_%H%M%S')
iteration = 0
start_epoch = 1
if(not os.path.exists(os.path.join(args.save_dir, args.network, start_time))):
    os.makedirs(os.path.join(args.save_dir, args.network, start_time))

random.seed(args.manual_seed)
np.random.seed(args.manual_seed)
torch.manual_seed(args.manual_seed)
cudnn.benchmark = False
torch.backends.cudnn.deterministic = True


if args.dataset=='generate':
    train_dataset = generateDataset(in_channel=args.in_channel, transform=train_transforms(height=args.height))
    # train_dataset = generateDataset(albu_transform = get_augumentation(phase='train', height=args.height))
    valid_dataset = ocrDataset(root_dir=args.root_dir, file_name='data.csv', in_channel=args.in_channel, transform=test_transforms(height=args.height))
else:
    args.in_channel = 3
    train_dataset = ocrDataset(root_dir=args.root_dir, file_name='./data.csv', transform=train_transforms(height=args.height))
    valid_dataset = ocrDataset(root_dir=args.root_dir, file_name='./data.csv', transform=test_transforms(height=args.height))


train_loader = torch.utils.data.DataLoader(
    train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=int(args.num_worker), collate_fn=alignCollate())
valid_loader = torch.utils.data.DataLoader(
    valid_dataset, batch_size=args.batch_size, shuffle=False, num_workers=int(args.num_worker), collate_fn=alignCollate())

checkpoint = []
if args.resume is not None:
    print('loading pretrained class from {}'.format(args.resume))
    checkpoint = torch.load(
        args.resume, map_location=lambda storage, loc: storage)
    params = checkpoint['parser']
    args.num_class = params.num_Class 
    args.in_channel = params.in_channel 
    args.height = params.height 
    args.network = params.network
    start_epoch = checkpoint['epoch'] + 1

if args.dataset!='generate':
    args.alphabet = get_vocab(root_dir=args.root_dir, file_name='./data.csv')

args.num_class = len(args.alphabet) + 1
print('parser: ', args)
converter = strLabelConverter(args.alphabet)

# model = ocrModel(num_classes=args.num_class, in_channel=args.in_channel, fixed_height=args.height, network=args.network)
model = denModel(n_classes=args.num_class, in_channel=args.in_channel, fixed_height=args.height)
optimizer = optim.Adam(model.parameters(), lr=args.lr, betas=(0.5, 0.999))
criterion = CTCLoss()
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=3, verbose=True)

if args.resume is not None:
    model.load_state_dict(checkpoint['state_dict'])
    del checkpoint

global image, text, length
image = torch.FloatTensor(args.batch_size, 3, args.height, 500)
text = torch.LongTensor(args.batch_size * 10)
length = torch.LongTensor(args.batch_size)

if(torch.cuda.is_available()):
    print('Run on cuda device')
    model = model.cuda()
    image = image.cuda()
    text = text.cuda()
    criterion = criterion.cuda()

image = Variable(image)
text = Variable(text)
length = Variable(length)


def train(data_loader):
    total_loss = []
    total_time = []
    model.train()
    start_time = time.time()
    for idx, (cpu_images, cpu_texts) in enumerate(data_loader):
        batch_size = cpu_images.size(0)
        loadData(image, cpu_images)
        t, l = converter.encode(cpu_texts)
        loadData(text, t)
        loadData(length, l)
        output = model(image)
        input_lengths = torch.full(size=(batch_size,), fill_value=output.size(0), dtype=torch.long)
        target_lengths = length.long()
        loss = criterion(output, text, input_lengths, target_lengths)
        optimizer.zero_grad()
        loss.backward()
        clipping_value = 1.0
        torch.nn.utils.clip_grad_norm_(model.parameters(), clipping_value)
        if not (torch.isnan(loss) or torch.isinf(loss)):
            optimizer.step()
        else:
            print("optimizer not step at {}".format(idx))
        total_loss.append(loss.item())
        total_time.append(time.time()-start_time)
        start_time = time.time()
        if idx % 100 == 0:
            print("iters: {}/{}=={}%,  {}s/batch, loss= {}".format(idx, len(data_loader), round(idx*100/len(data_loader)), round(np.mean(total_time), 2), round(np.mean(total_loss), 3)))
    return np.mean(total_loss)


def evaluate(data_loader):
    model.eval()
    total_loss = []
    accBF = 0.0
    accBC = 0.0
    with torch.no_grad():
        for idx, (cpu_images, cpu_texts) in enumerate(data_loader):
            batch_size = cpu_images.size(0)
            loadData(image, cpu_images)
            t, l = converter.encode(cpu_texts)
            loadData(text, t)
            loadData(length, l)
            output = model(image)
            output_size = Variable(torch.LongTensor([output.size(0)] * batch_size))
            loss = criterion(output, text, output_size, length)
            total_loss.append(loss.item())
            _, output = output.max(2)
            output = output.transpose(1, 0).contiguous().view(-1)
            sim_preds = converter.decode(output.data, output_size.data, raw=False)
            accBF += by_field(sim_preds, cpu_texts)
            accBC += by_char(sim_preds, cpu_texts)
        return np.mean(total_loss), accBF/len(data_loader), accBC/len(data_loader)


if __name__ == '__main__':
    by_field_best = 0.0
    for epoch in range(start_epoch, args.num_epoch):
        print("{} epoch: \t start training....".format(epoch))
        start = time.time()
        loss = train(train_loader)
        scheduler.step(loss)
        train_results = {'epoch': epoch,
                        'loss': loss,
                        'time': time.time()-start}
        for key, value in train_results.items():
            print('    {:15s}: {}'.format(str(key), value))
        if (epoch+1) % 2 != 0:
            continue
        print('{} epoch: \t start validation....'.format(epoch))
        val_loss, val_by_field, val_by_char = evaluate(valid_loader)
        model_best = False
        if val_by_field > by_field_best:
            model_best = True
            by_field_best = val_by_field 
        valid_results = {
            'epoch': epoch,
            'valid_loss': val_loss,
            'accuracy by field': val_by_field,
            'accuracy by char': val_by_char
        }
        for key, value in valid_results.items():
            print('    {:15s}: {}'.format(str(key), value))
        save_checkpoint(args, epoch, model, args.save_dir, model_best, start_time)