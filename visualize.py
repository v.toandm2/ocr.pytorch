from trdg.generators import (
    GeneratorFromDict,
    GeneratorFromRandom,
    GeneratorFromStrings,
    GeneratorFromWikipedia,
)
import numpy as np 
import matplotlib.pyplot as plt 
# The generators use the same arguments as the CLI, only as parameters

words = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
rand = lambda x: np.random.randint(len(x))
def random_string():
    return numbers[rand(numbers)]+numbers[rand(numbers)]+'-'+numbers[rand(numbers)]+numbers[rand(numbers)]+'-'+numbers[rand(numbers)]+numbers[rand(numbers)]+numbers[rand(numbers)]+numbers[rand(numbers)]+' '+words[rand(words)] + ' '+ numbers[rand(numbers)]+numbers[rand(numbers)]+':'+numbers[rand(numbers)]+numbers[rand(numbers)]+':'+numbers[rand(numbers)]+numbers[rand(numbers)]

generator = GeneratorFromStrings(
    strings = [random_string()],
    size = 64,
    skewing_angle=10,
    random_skew=True,
    blur=1,
    random_blur=True,
    background_type=np.random.randint(6),
    fit=True
)
img, label = generator.next()
img = img.convert('L')
print(img.size)
print('label: ', label)
plt.imshow(img)
plt.show()
