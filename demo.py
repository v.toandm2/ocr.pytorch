import cv2
import torch
import cv2
import numpy as np
from PIL import Image
from datasets import ocrDataset, alignCollate, test_transforms, train_transforms
from models import model
from torch.autograd import Variable
from utils import strLabelConverter
from datasets import generateDataset

# dataset = ocrDataset(root_dir='/home/toandm2/code/ocr_dataset/', file_name='./data.csv', transform=get_augumentation(phase='train'))
weight = './checkpoint_epoch114.pth'
if weight is not None:
    print('Load checkpoint ...')
    checkpoint = torch.load(weight, map_location=lambda storage, loc: storage)
    state_dict = checkpoint['state_dict']
    args = checkpoint['parser']
num_class = len(args.alphabet) + 1
# args.in_channel = 3
args.height = 64
model = model.ocrModel(num_classes=args.num_class, in_channel=args.in_channel,
                       fixed_height=args.height, network=args.network)
model.load_state_dict(state_dict)
converter = strLabelConverter(args.alphabet)


def inference(img):
    model.eval()
    with torch.no_grad():
        img = img.unsqueeze(0)
        output = model(img)
        output_size = Variable(torch.LongTensor([output.size(0)] * 1))
        _, output = output.max(2)
        output = output.transpose(1, 0).contiguous().view(-1)
        sim_preds = converter.decode(output.data, output_size.data, raw=False)
        return sim_preds


dataset = generateDataset(transform=test_transforms(args.height))


cap = cv2.VideoCapture(
    'rtsp://admin:123456789abc@192.168.1.64:554/Streaming/Channels/101/')
cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
transform = train_transforms(height=args.height)
idx = 0
while(True):
    # Capture frame-by-frame
    ret, img = cap.read()
    x1 = 660
    y1 = 980
    x2 = 1420
    y2 = 1050
    # img[y1:y2, x1:x2] = img[y1-100:y2-100, x1:x2]
    # img[935:980, 1350:1826] = img[935-100:980-100, 1350-100:1826-100]
    # cv2.imwrite('./pictures/frame{}.png'.format(idx), img)
    # idx += 1
    cut_img = img[y1:y2, x1:x2]
    cv2.imshow('frame', cut_img)
    cut_img = cv2.cvtColor(cut_img, cv2.COLOR_BGR2GRAY)
    cut_img = transform(np.array(cut_img))
    predict = inference(cut_img)

    cv2.rectangle(img, (x1, y1), (x2, y2), (179, 255, 179), 2, 1)
    labelSize, baseLine = cv2.getTextSize(
        predict, cv2.FONT_HERSHEY_SIMPLEX, 0.8, 2)
    cv2.rectangle(
        img, (x1, y1-labelSize[1]), (x1+labelSize[0], y1+baseLine), (223, 128, 255), cv2.FILLED)
    cv2.putText(img, predict, (x1, y1),
                cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 2)
    # Display the resulting frame
    cv2.imshow('Image', img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
# for i in range(10):
#     img, label = dataset[i]
#     text = inference(img)
#     cv2.imshow(text, img[0].numpy())
#     cv2.waitKey(-1)
# cv2.destroyAllWindows()
